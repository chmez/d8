# Installation profile for Drupal 8/9

[![Made in Ukraine](https://img.shields.io/badge/made_in-ukraine-ffd500.svg?labelColor=005bbb)](https://supportukrainenow.org)
[![Version](http://poser.pugx.org/chmez/d8/version)](https://packagist.org/packages/chmez/d8)
[![Total Downloads](http://poser.pugx.org/chmez/d8/downloads)](https://packagist.org/packages/chmez/d8)

## Mirror

[GitLab](https://gitlab.com/chmez/d8)

## Requirements

This installation profile requires the following modules:

* [Admin Toolbar](https://www.drupal.org/project/admin_toolbar)
* [Automatic IP ban (Autoban)](https://www.drupal.org/project/autoban)
* [Bootstrap](https://www.drupal.org/project/bootstrap) (theme)
* [CAPTCHA](https://www.drupal.org/project/captcha)
* [Config Export to PHP array](https://www.drupal.org/project/config2php)
* [Config override](https://www.drupal.org/project/config_override)
* [Configuration Update Manager](https://www.drupal.org/project/config_update)
* [Features](https://www.drupal.org/project/features)
* [Module Filter](https://www.drupal.org/project/module_filter)
* [reCAPTCHA](https://www.drupal.org/project/recaptcha)
* [reCAPTCHA Preloader](https://www.drupal.org/project/recaptcha_preloader)
* [StandWithUkraine 🇺🇦](https://www.drupal.org/project/standwithukraine)

## Features

### New installation step for setting captcha keys

![captcha](images/captcha.png "captcha")

### Maintenance mode

![maintenance](images/maintenance.png "maintenance")
