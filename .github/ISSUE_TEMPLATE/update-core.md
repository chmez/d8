---
name: Update core
about: Update core.
title: Update core from 3.CURRENT to 3.LAST
labels: update, core
assignees: ''

---

Update [core](https://www.drupal.org/project/drupal) from **3.CURRENT** to **3.LAST**. Compare versions [here](https://git.drupalcode.org/project/drupal/-/compare/9.3.CURRENT...9.3.LAST)
